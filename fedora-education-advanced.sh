#!/bin/bash

echo "Install required dependencies (ROOT)"
sudo dnf install livecd-tools spin-kickstarts genisoimage

echo "Create custom ISO image (ROOT)"
sudo livecd-creator --verbose \
       --fslabel=FedoraEduAdvanced \
       --releasever=26 \
       --config=fedora-live-education-advanced.ks
