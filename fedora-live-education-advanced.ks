%include fedora-live-lxqt.ks

%packages

# Accrete is a physical simulation of solar system planet formation
accrete

# Grammar development environment for ANTLR v3 grammars
antlrworks

# Adaptive Poisson Boltzmann Solver
apbs

# Astronomy menu for the Desktop
astronomy-menus

# Advanced molecular editor
avogadro2

# 3D modeling, animation, rendering and post-production
blender

# A program for 2D drawing organic molecules
chemtool

# Utility to organize and develop raw images
darktable

# Diagram drawing program
dia

# IDE for various programming languages
eclipse

# Web browser
firefox

# A general purpose 3D CAD modeler
freecad

# A general chemistry lab experiment simulator
genchemlab

# Molecular mechanics and quantum mechanics front end for GNOME
ghemical

# GNU Image Manipulation Program
gimp

# Various Gnome Chemistry Programs
gnome-chemistry-utils

# A tool for econometric analysis
gretl

# Vector-based drawing program using SVG
inkscape

# A free open-source molecular editor and visualization package
IQmol

# A 3D Modeling, Animation and Rendering System
k3d

# 2D and 3D Graph Calculator
kalgebra

# Periodic Table of Elements
kalzium

# Geography Trainer
kgeography

# Interactive Geometry
kig

# Krita is a sketching and painting program
krita

# Desktop Planetarium
kstars

# Data Analysis and Visualization
LabPlot

# A set of writing, drawing, compute data, programs
libreoffice

# Texas Instruments calculator communication library
libticalcs2

# Linux MultiMedia Studio
lmms

# Educational tool for simulating digital logic circuits
logisim

# Virtual globe and world atlas
marble

# An interactive development environment for programming in MIPS assembly language
Mars

# Small, portable symbolic math program
mathomatic

# A system for processing and editing unstructured 3D triangular meshes
meshlab

# Open Source Mass Spectrometry Tool
mMass

# Nightfall is an astronomy application for emulation of eclipsing stars
nightfall

# Midi/Audio Music Sequencer
muse

# A high-level language for numerical computations
octave

# A geometry software that is intended for educational or modeling purposes
openeuclide

# Orbit Reconstruction, Simulation and Analysis
orsa

# Application for protein structure, dynamics and sequence analysis
ProDy

# A program for statistical analysis of sampled data
pspp

# Powerful 2D CAD system
qcad

# Graphical frontend for R language
rkward

# Graph Theory IDE
rocs

# Scientific software package for numerical computations
scilab

# Astronomical image processing software
siril

# Music education software
solfege

# Scientific Python Development Environment
spyder

# Photo-realistic nightsky renderer
stellarium

# Interactive Physics Simulator
step

# Vector-based 2D animation studio (broken dependencies)
#synfigstudio

# Email client
thunderbird

# A fast, portable real-time interactive fractal zoomer
xaos

# 2D chemical structures drawing tool
xdrawchem

%end
