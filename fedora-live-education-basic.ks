%include fedora-live-lxqt.ks

%packages

# Interactive educational geometry software
drgeo

# Web browser
firefox

# Educational suite for kids 2-10 years old
gcompris

# Practice Fractions
kbruch

# Geography Trainer
kgeography

# Interactive Geometry
kig

# Learn the alphabet and read some syllables in different languages
klettres

# Touch Typing Tutor
ktouch

# Educational Programming Environment
kturtle

# A set of writing, drawing, compute data, programs
libreoffice

# Texas Instruments calculator communication library
libticalcs2

# Educational tool for simulating digital logic circuits
logisim

# A geometry software that is intended for educational or modeling purposes
openeuclide

# Email client
thunderbird

# Educational math tutor for children
tuxmath

# Drawing program designed for young children
tuxpaint

# Tux Typing, an educational typing tutor for children
tuxtype2

%end
